import Swiper from 'swiper/dist/js/swiper.js'

export default function wishSlider() {
  const slider = document.querySelector('.wishItems')
  if (slider) {
    let swiper = new Swiper('.wishItems', {
      slidesPerView: 6,
      spaceBetween: 20,
      loop: false,
      scrollbar: '.swiper-scrollbar',
      scrollbarHide: false,
      breakpoints: {
        1715: {
          slidesPerView: 5,
        },
        1450: {
          slidesPerView: 4,
        },
        1100: {
          slidesPerView: 3,
        },
        900: {
          slidesPerView: 2,
        },
      },
    })
  }
}
