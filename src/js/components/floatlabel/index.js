export default function floatlabel() {
  const elements = Array.from(document.querySelectorAll('.uiTextInputGroup'))
  if (elements) {
    elements.map(element => {
      let input = element.querySelector('input')
      if (input === null) input = element.querySelector('textarea')

      let placeholder = input.placeholder
      input.placeholder = ''

      if (input.value) {
        element.classList.add('uiTextInputGroupFocus')
        input.placeholder = placeholder
      }

      input.addEventListener('focus', () => {
        element.classList.add('uiTextInputGroupFocus')
        input.placeholder = placeholder
      })

      input.addEventListener('blur', () => {
        if (!input.value) {
          element.classList.remove('uiTextInputGroupFocus')
          input.placeholder = ''
        }
      })
    })
  }
}
