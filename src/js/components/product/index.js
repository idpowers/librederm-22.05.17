import Swiper from 'swiper/dist/js/swiper.js'

export default function productSlider() {
  // let swiperProduct = ''
  // let swiperThumbProduct = ''
  const sliderProduct = document.querySelector('.productImageSlider')
  // const sliderThumbProduct = document.querySelector('.productImageSliderThumbs')

  if (document.contains(sliderProduct)) {
    const swiperProduct = new Swiper('.productImageSlider', {
      slidesPerView: 1,
      spaceBetween: 0,
      effect: 'fade',
      loop: true,
      fade: {
        crossFade: true,
      },
      loopedSlides: 4,
    })

    const swiperThumbProduct = new Swiper('.productImageSliderThumbs', {
      slidesPerView: 5,
      spaceBetween: 0,
      loop: true,
      slideToClickedSlide: true,
      nextButton: '.productImageSliderThumbRight',
      prevButton: '.productImageSliderThumbLeft',
      loopedSlides: 4,
      breakpoints: {
        1715: {
          slidesPerView: 4,
        },
      },
    })

    swiperProduct.params.control = swiperThumbProduct
    swiperThumbProduct.params.control = swiperProduct
  }

  // if (document.contains(sliderProduct)) {
  //   var slideControls = Array.from(document.querySelectorAll('.productImageSliderThumb'))
  //   slideControls.map(function(el, i) {
  //     el.addEventListener('click', function() {
  //       slideControls.map(item => {
  //         item.classList.remove('productImageSliderThumbActive')
  //       })
  //       swiperProduct.slideTo(i + 1)
  //       el.classList.add('productImageSliderThumbActive')
  //     })
  //   })
  // }
}
