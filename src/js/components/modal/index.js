export default function modalInit() {
  const modalElements = Array.from(document.querySelectorAll("[data-modal]"))
  modalElements.map(el => {
    const modalId = el.dataset.modal
    const modal = document.querySelector(modalId)
    const modalClose = modal.querySelector(".modalClose")
    const modalClose2 = modal.querySelector(".js-modal-close")
    const overlay = document.querySelector(".overlay")
    el.addEventListener("click", () => {
      showOverlay()
      modal.classList.add("modalShow")
      modalClose.addEventListener("click", () => {
        modal.classList.remove("modalShow")
        setTimeout(() => {
          hideOverlay()
        }, 300)
      })
      overlay.addEventListener("click", () => {
        modal.classList.remove("modalShow")
        setTimeout(() => {
          hideOverlay()
        }, 300)
      })
      // modalClose2.addEventListener('click', () => {
      //   modal.classList.remove('modalShow')
      //   setTimeout(() => {
      //     hideOverlay()
      //   }, 300)
      // })
    })
  })

  const modalCartButton = Array.from(document.querySelectorAll(".headerCartIcon"))
  const modalCart = document.querySelector(".modalCart")
  modalCartButton.map(el => {
    el.addEventListener("click", () => {
      showOverlay()
      const modalClose = modalCart.querySelector(".modalClose")
      const overlay = document.querySelector(".overlay")
      modalCart.classList.add("modalCartIsShow")
      modalClose.addEventListener("click", () => {
        modalCart.classList.remove("modalCartIsShow")
        setTimeout(() => {
          hideOverlay()
        }, 300)
      })
      overlay.addEventListener("click", () => {
        modalCart.classList.remove("modalCartIsShow")
        setTimeout(() => {
          hideOverlay()
        }, 300)
      })
    })
  })
}

const showOverlay = () => {
  const overlay = document.querySelector(".overlay")
  const body = document.querySelector("body")
  overlay.classList.add("overlayShow")
  body.classList.add("modalBodyShow")
}

const hideOverlay = () => {
  const overlay = document.querySelector(".overlay")
  const body = document.querySelector("body")
  overlay.classList.remove("overlayShow")
  body.classList.remove("modalBodyShow")
}
